/******************************************************************************
 * Copyright (c) 2013 Robin O'Brien.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Robin O'Brien - initial implementation 
 *    
 ****************************************************************************/

package org.urdad.validation;

public class Main {

	private static String URDADMetaModelPath;
	private static String URDADModelPath;
	private static String OCLFilePath;
	private static String newOCLFilePath;

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		/*
		 * Pre: Handle missing parameters
		 */
		try {
			processParameters(args);
		} catch (IllegalArgumentException e) {
			System.out.println("Missing Parameters! Usage:\n");
			System.out
					.println("java URDADModelValidator"
							+ " <metaModelPath> <modelPath> <oclConstraintsPath> <pathToNewOCLFile>");
			System.exit(1);
		}

		/*
		 * Step 1: Initialise the URDAD Model Validator
		 */
		URDADModelValidator URDADValidator;
		if (newOCLFilePath == null) {
			URDADValidator = new URDADModelValidator(URDADMetaModelPath,
					URDADModelPath, OCLFilePath);
		} else {
			URDADValidator = new URDADModelValidator(URDADMetaModelPath,
					URDADModelPath, OCLFilePath, newOCLFilePath);
		}

		/*
		 * Step 2: Perform the validation and store the result
		 */
		String result = URDADValidator.validate();

		/*
		 * Step 3: Output the validation result
		 */
		System.out.println(result);

	}

	/**
	 * Processes the given parameters. Checks for missing parameters and stores
	 * the parameters in global variables for later use
	 * 
	 * @param args
	 *            Arguments supplied when program is run
	 * @return boolean, true if successful
	 */
	private static boolean processParameters(String[] args)
			throws IllegalArgumentException {

		if (args.length < 3) {
			// If the arguments are missing, throws an exception
			throw new IllegalArgumentException("Missing arguments");
		} else if (args.length < 4) {
			// If the arguments are present, stores the values
			URDADMetaModelPath = args[0];
			URDADModelPath = args[1];
			OCLFilePath = args[2];
			return true;
		} else {
			// If the arguments are present, stores the values
			URDADMetaModelPath = args[0];
			URDADModelPath = args[1];
			OCLFilePath = args[2];
			newOCLFilePath = args[3];
			return true;
		}
	}
}
