/******************************************************************************
 * Copyright (c) 2013 Robin O'Brien.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Robin O'Brien - initial implementation 
 *    
 ****************************************************************************/

package org.urdad.validation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EValidator;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.ocl.examples.pivot.manager.MetaModelManager;
import org.eclipse.ocl.examples.pivot.model.OCLstdlib;
import org.eclipse.ocl.examples.pivot.utilities.PivotUtil;
import org.eclipse.ocl.examples.xtext.completeocl.CompleteOCLStandaloneSetup;
import org.eclipse.ocl.examples.xtext.completeocl.validation.CompleteOCLEObjectValidator;

/**
 * URDAD Model Validator, validates an given URDAD model in XMI format using a
 * given OCL file containing all the meta-models constraints.
 * 
 * @author Robin O'Brien
 * 
 */
public class URDADModelValidator {

	private static URI URDADMetaModelURI;
	private static URI URDADModelURI;
	private static URI OCLFileURI;

	private static File newOCLFile;

	private Resource URDADResource;

	/**
	 * Default constructor
	 * 
	 * @param URDADMetaModelPath
	 *            path to URDAD meta model ecore file
	 * @param URDADModelPath
	 *            path to URDAD model xmi file
	 * @param OCLFilePath
	 *            path to URDAD models OCL file
	 */
	public URDADModelValidator(String URDADMetaModelPath,
			String URDADModelPath, String OCLFilePath) {

		URDADMetaModelURI = URI.createFileURI(new File(URDADMetaModelPath)
				.getAbsolutePath());
		URDADModelURI = URI.createFileURI(new File(URDADModelPath)
				.getAbsolutePath());
		OCLFileURI = URI.createFileURI(new File(OCLFilePath).getAbsolutePath());

		// Step 2.1: Initialize OCL resources
		CompleteOCLStandaloneSetup.doSetup();
		OCLstdlib.install();

	}

	/**
	 * Alternative constructor
	 * 
	 * Allows for specification of the path to a new ocl file
	 * 
	 * @param URDADMetaModelPath
	 *            path to URDAD meta model ecore file
	 * @param URDADModelPath
	 *            path to URDAD model xmi file
	 * @param OCLFilePath
	 *            path to URDAD models OCL file
	 * @param newOCLFilePath
	 *            path to save a new derived ocl file
	 */
	public URDADModelValidator(String URDADMetaModelPath,
			String URDADModelPath, String OCLFilePath, String newOCLFilePath) {

		newOCLFile = new File(newOCLFilePath);

		URDADMetaModelURI = URI.createFileURI(new File(URDADMetaModelPath)
				.getAbsolutePath());
		URDADModelURI = URI.createFileURI(new File(URDADModelPath)
				.getAbsolutePath());
		OCLFileURI = URI.createFileURI(new File(OCLFilePath).getAbsolutePath());

		// Step 2.1: Initialize OCL resources
		CompleteOCLStandaloneSetup.doSetup();
		OCLstdlib.install();

	}

	/**
	 * Performs the URDAD Model Validation
	 * 
	 * @return a string containing human readable output
	 */
	public String validate() {

		// Step 1: Get the root object of the URDAD model
		EObject rootObject = getURDADRootObject();
		System.out.println("Root object loaded ("
				+ rootObject.eClass().getName() + ")");

		// Step 2: Perform Diagnostics
		Diagnostic diagnostics = Diagnostician.INSTANCE.validate(rootObject);

		// Step 3: Print results
		return processDiagnosticResults(diagnostics);
	}

	/**
	 * Creates a resource set object
	 * 
	 * @return the resource set
	 */
	private ResourceSet createResourceSet() {

		// Create a resource set that will hold all the resources
		ResourceSet resourceSet = new ResourceSetImpl();

		// Register resource factory to handle ecore models
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
				.put("ecore", new EcoreResourceFactoryImpl());
		// Register resource factory to handle xmi models
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
				.put("library", new XMIResourceFactoryImpl());
		// Register resource factory to handle xmi models
		resourceSet
				.getResourceFactoryRegistry()
				.getExtensionToFactoryMap()
				.put(Resource.Factory.Registry.DEFAULT_EXTENSION,
						new XMIResourceFactoryImpl());

		return resourceSet;
	}

	/**
	 * Registers a package in the EPackage registry
	 * 
	 * @param ePackage
	 *            EPackage to register
	 * @param fileURI
	 *            URI representing path to the file
	 */
	private void registerPackage(EPackage ePackage, final URI fileURI) {

		// If all the values are present and we haven't added the package
		// already
		if ((ePackage != null && fileURI != null && ePackage.getNsURI() != null)
				&& !EPackage.Registry.INSTANCE.containsKey(ePackage.getNsURI())) {
			EPackage.Registry.INSTANCE.put(ePackage.getNsURI(), ePackage);
			URIConverter.URI_MAP.put(URI.createURI(ePackage.getNsURI()),
					fileURI);
		}
	}

	/**
	 * Iterates through a given packages and adds all its sub packages
	 * 
	 * @param ePackage
	 *            Package that may contain sub packages
	 * @param fileURI
	 *            URI representing path to file
	 */
	private void registerSubPackages(EPackage ePackage, URI fileURI) {

		if ((ePackage != null && fileURI != null)) {
			for (TreeIterator<EObject> iterator = ePackage.eAllContents(); iterator
					.hasNext();) {
				EObject obj = iterator.next();
				if (obj instanceof EPackage) {
					EPackage _package = (EPackage) obj;
					registerPackage(_package, fileURI);
				}
			}
		} else {
			throw new IllegalArgumentException();
		}

		System.out.println();

	}

	/**
	 * Extracts the top level URDAD EObject from the available files
	 * 
	 */
	private EObject getURDADRootObject() {

		// Step 1.1: Create the resource set
		ResourceSet URDADResourceSet = createResourceSet();

		// Step 1.2: Get the URDAD resource
		URDADResource = URDADResourceSet.getResource(URDADMetaModelURI, true);

		// Step 1.3: Get the top level package from the URDAD resource
		EPackage topLevelPackage = (EPackage) URDADResource.getContents()
				.get(0);

		// Step 1.4: Register the top level package as well as the nested
		// packages
		registerPackage(topLevelPackage, URDADMetaModelURI);
		registerSubPackages(topLevelPackage, URDADMetaModelURI);

		// Step 1.5: Get the resource for the URDAD model instance
		Resource urdadModel = URDADResourceSet.getResource(URDADModelURI, true);

		// Step 1.6: Create a prepared OCL File containing only relevant
		// constraints (invariants) (Exluded for now)
		// createOCLFile(topLevelPackage.getName());

		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(
				Resource.Factory.Registry.DEFAULT_EXTENSION,
				new XMIResourceFactoryImpl());

		// Step 1.7 Register validators for each package
		registerValidators(topLevelPackage);

		// Step 1.8: Get the URDAD root object from the model
		EObject URDADRootObject = urdadModel.getContents().get(0);

		return URDADRootObject;
	}

	/**
	 * Registers a CompleteOCLEObjectValidator for the given package and its
	 * sub-packages.
	 * 
	 * @param topLevelPackage
	 *            Highest level package to register the validator against
	 */
	private void registerValidators(EPackage topLevelPackage) {
		// Step 1.7.1: Create a MetaModelManager
		MetaModelManager URDADMetaModelManager = PivotUtil
				.getMetaModelManager(URDADResource);

		// Step 1.7.2: Initialise and register the OCL validator
		CompleteOCLEObjectValidator myValidator = new CompleteOCLEObjectValidator(
				topLevelPackage.getESubpackages().get(0), OCLFileURI,
				URDADMetaModelManager);

		// Step 1.7.3: Iterate throught the package tree and register validators
		EValidator.Registry.INSTANCE.put(topLevelPackage, myValidator);

		for (TreeIterator<EObject> iterator = topLevelPackage.eAllContents(); iterator
				.hasNext();) {

			EObject obj = iterator.next();
			if (obj instanceof EPackage) {
				EPackage _package = (EPackage) obj;
				EValidator.Registry.INSTANCE.put(_package, myValidator);
			}
		}

	}

	/**
	 * Creates temporary constraints file based on the original one supplied
	 * 
	 * @param packageName
	 *            name of the root package
	 */
	@SuppressWarnings("unused")
	private void createOCLFile(String packageName) {

		String currentString;
		ArrayList<String> OCLFileContent = new ArrayList<String>();

		// add import statement describing the location of the URDAD meta-model
		// add name of the package
		OCLFileContent.add("import '" + URDADMetaModelURI.path().toString()
				+ "'");

		// OCLFileContent.add("package " + packageName);

		int counter = 1;

		// set up reader to read through the OCL file
		BufferedReader in;
		try {
			in = new BufferedReader(new FileReader(OCLFileURI.path()));

			// Loop while there are new lines
			while ((currentString = in.readLine()) != null) {

				if (currentString.trim().startsWith("package")) {
					OCLFileContent.add(currentString);
				} else if (currentString.trim().startsWith("context")) {
					// add it to the new ocl file
					OCLFileContent.add(currentString);
				} else if (currentString.trim().startsWith("inv")) {
					// add invariant to new file
					String newStr = currentString.replace("inv:",
							"inv Constraint_" + counter + ": ");
					counter++;
					OCLFileContent.add(newStr);
				} else if ((currentString.contains("self") && (!currentString
						.contains("tself") || currentString.contains("mself") || currentString
							.contains("rself")))
						|| currentString.contains(".ocl")) {
					if (currentString.contains(":")) {
						// Assume it has a name
						OCLFileContent.add(currentString);
					} else {
						// add the constraint with an assigned name
						OCLFileContent.add("inv Constraint_" + counter + ": "
								+ currentString);
						counter++;
					}

				}
			}

			in.close();
			OCLFileContent.add("endpackage");

			BufferedWriter out = new BufferedWriter(new FileWriter(newOCLFile));

			for (String s : OCLFileContent) {
				out.write(s + "\n");
			}

			out.close();
		} catch (FileNotFoundException e) {
			System.out.println("OCL File not found! : " + e);
		} catch (IOException e) {
			System.out.println("Error writing to new OCL File! : " + e);
		}

	}

	/**
	 * Processes the results of the validation process.
	 * 
	 * @param diagnostic
	 *            The diagnostic object containing the results of the validation
	 * @return String contained human readable output
	 */
	private String processDiagnosticResults(Diagnostic diagnostic) {

		String results = "";

		if (diagnostic.getChildren().isEmpty()) {
			// All constraints passed. Model is valid
			results += "All constraints for the model are valid";
		} else {
			results += "Validation Failed: Constraints are invalid";
			for (Object object : diagnostic.getChildren()) {
				results += "\n" + object.toString();
			}
		}
		return results;
	}

}
